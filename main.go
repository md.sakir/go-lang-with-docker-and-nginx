// app/main.go

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func handler(w http.ResponseWriter, r *http.Request) {
	// Create a directory structure for log files based on the date
	logDir := "logs/" + time.Now().Format("2006-01-02")
	if err := os.MkdirAll(logDir, os.ModePerm); err != nil {
		log.Printf("Error creating log directory: %v", err)
	}

	// Generate a log file name based on the current hour
	logFile := logDir + "/" + time.Now().Format("15.log")
	logOutput, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Printf("Error opening log file: %v", err)
	}

	// Close the log file when done
	defer logOutput.Close()
	log.SetOutput(logOutput)

	// Log the request details
	log.Printf("[INFO] [%s] %s %s", time.Now().Format("2006-01-02 15:04:05"), r.Method, r.URL.Path)
	// Log a custom message
	log.Printf("[INFO] Custom message: This is a custom log message\n\n\n")

	fmt.Fprintf(w, "Hello, Go with Docker and Nginx 234!\n")
}


func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
