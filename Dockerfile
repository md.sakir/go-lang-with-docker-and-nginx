# Stage 1: Build the Go application
FROM golang:1.16 AS builder

# Disable Go modules temporarily
ENV GO111MODULE=off

WORKDIR /app
COPY main.go .

# Build the Go application
RUN CGO_ENABLED=0 GOOS=linux go build -o app

# Stage 2: Create a minimal image to run the Go application
FROM alpine:latest

# Copy the binary from the previous stage
COPY --from=builder /app/app /app/app

# Expose port 8080
EXPOSE 5052

# Command to run the application
CMD ["/app/app"]
